---
author:
- 'Antonio Nardella - progetti\@antonionardella.it'
title: |
    OCSInventory Agent Installation auf Debian GNU/Linux Geräte in einer
    FUSS-Umgebung
---

Einführung
==========

Dieses Dokument erklärt wie man OCSInventory Agent in einer
FUSS-Umgebung installiert. OCSInventory Agent wird als `.deb` Packet aus
`GitLab` runtergeladen und mit `gdebi` installiert. Das `.deb` Paket ist
von Michael von Guggenberg (a.k.a. Guggi) für Debian `jessie` und
`stretch` kompiliert worden.

Voraussetzungen
===============

-   Internet Anbindung

-   FUSS Server mit squid3

-   Editor Kenntnisse (nano, vi, GNU/Emacs)

-   OCSInventory Server Zertifikat (Bitte wenden Sie sich an den
    Systemadministrator oder den Verantwortlichen für OCSInventory um
    das Zertifikat zu bekommen.)

-   OCSInventory TAG zur Gruppierung der Gräte (Bitte wenden Sie sich an
    den Systemadministrator oder den Verantwortlichen für OCSInventory
    um das TAG zu bekommen.)

FUSS Server: squid3 Einstellungen
=================================

ACLs Einrichten
---------------

Die Installation und das OCSInventory Agent benötigen eine
Internetanbindung. Squid3 dient in einer FUSS-Umgebung als Proxy Server
und muss deshalb die Verbindung zu verschiedene Dienste ermöglichen.
Liste der nötigen Dienste/Seiten/Domäne:

-   inventory.snets.it

-   github.com

-   gitlab.com

-   s3.amazonaws.com

Verbindung zum FUSS-Server öffnen über SSH (Kommandozeile oder Putty)
und folgende Datei mit den Editor öffnen:
`vi /etc/squid3/squid-added-repo.conf` folgende Zeilen hinzufügen:

```
acl repositories url_regex inventory.snets.it  
acl repositories url_regex github.com  
acl repositories url_regex gitlab.com  
acl repositories url_regex s3.amazonaws.com
```

Schließen und speichern.

![ Geänderte /etc/squid3/squid-added-repo.conf Datei](squidAcls.png)

Den squid proxy neustarten mit:  
`systemctl reload squid3`

OCSInventory Agent über Skript installieren
===========================================

Server Zertifikat
-----------------

OCSInventory Agent benötigt ein Zertifikat für die Kommunikation mit den
OCSInventory Server. Bitte wenden Sie sich beim Systemadministrator oder
den Verantwortlichen für OCSInventory um das Zertifikat zu bekommen. Das
Zertifikat wird mit den folgenden Skript im
Ordner`/usr/share/szme-fuss-fog/ocs/server.crt` erwartet. Den Ordner
erstellen und das Zertifikat kopieren.

Das Zertifikat kann auch in anderen Ordnern liegen. In einem zweiten Moment die OCSInventory Agent konfiguration unter `/etc/ocsinventory/ocsinventory-agent.cfg` ändern und den korrekten Pfad bei `ca=` setzen.

Bash Skript
-----------

Zur automatisierten Installation des OCSInventory Agent steht ein Bash
Skript zur Verfügung. Den Skript mit folgenden Befehl runterladen:  
`wget https://gitlab.com/antonionardella/szme-ocsinventoryagent/raw/master/install.sh`

OCSInventory Agent nutzt den TAG zur Gruppierung der Geräte. Bitte wenden
Sie sich an den Systemadministrator oder den Verantwortlichen für
OCSInventory um das TAG zu bekommen.Dieser Skript erwartet den TAG als
Argument. z.B.: `TAG=251103` somit den Skript wie folgt ausführen:  
`bash install.sh 251103`

Der Skript installiert die nötigen Packete wie z.B.
`pciutils, smartmontools, read-edid, nmap`, lädt je nach Architektur und
GNU/Debian Release das nötige `.deb` Paket runter, installiert es und
setzt die Konfiguration für OCSInventory Agent fort.

Feedback und Support
--------------------

Bei weitere Fragen oder Verbesserungsvoschläge zum Skript oder dieser
Dokumentation bitte eine Issue auf GitLab öffnen:  
`https://gitlab.com/antonionardella/szme-ocsinventoryagent/issues`
