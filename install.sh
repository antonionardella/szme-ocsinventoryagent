#!/bin/bash
#### Filename: installOcsInventoryAgent.sh
#### Creation Date: 2019-04-05T14:55:10+02:00
####
#### Last modified by:   antonio
#### Last modified time: 2019-04-05T14:55:13+02:00
####
#### Copyright: 2019 Antonio Nardella <antonio> - progetti@antonionardella.it
####
#### SPDX-License-Identifier: GPL-3.0-or-later
#### ##############################
#### ##          WARNING         ##
#### ##############################
####
#### TO USE THIS SCRIPT
####
#### 1. SET THE TAG IN LINE 33
#### 2. IN A FUSS ENVIRONMENT ADD FOLLOWING ACLs TO SQUID
####
#### acl repositories url_regex inventory.snets.it
#### acl repositories url_regex github.com
#### acl repositories url_regex s3.amazonaws.com
####
#### to /etc/squid3/squid.conf or /etc/squid3/squid-added-repo.conf
####
#### and reload the configuration
####
#### systemctl reload squid3
####
#### ##############################
#### ##          WARNING         ##
#### ##############################

# Get OCSInventory Tag from user input or hardcode
TAG=$1

# Install packages
DEBIAN_FRONTEND=noninteractive DEBIAN_PRIORITY=critical sudo apt-get -q -y -o "Dpkg::Options::=--force-confdef" -o "Dpkg::Options::=--force-confold" install gdebi dmidecode libxml-simple-perl libcompress-zlib-perl libnet-ip-perl libwww-perl libdigest-md5-perl libnet-ssleay-perl libcrypt-ssleay-perl libnet-snmp-perl libproc-pid-file-perl libproc-daemon-perl net-tools libsys-syslog-perl pciutils smartmontools read-edid nmap

# Install OCSInventory agent
echo "Install OCSInventory agent"
ARCHITECTURE=$(dpkg --print-architecture)
RELEASE=$(lsb_release -c | cut -f2)
wget -O ocsinventory-agent_2.4.2-3_bpo9+1_$ARCHITECTURE.deb  https://gitlab.com/antonionardella/szme-fuss-fog/raw/master/packages/ocs/$RELEASE/ocsinventory-agent_2.4.2-3_bpo9+1_$ARCHITECTURE.deb?inline=false

# Install OCSInventory agent unattended
DEBIAN_FRONTEND=noninteractive gdebi --non-interactive ocsinventory-agent_2.4.2-3_bpo9+1_$ARCHITECTURE.deb

# Show Warning to copy server.crt to specific folder
mkdir -p /usr/share/szme-fuss-fog/ocs/
wget -P /usr/share/szme-fuss-fog/ocs/ -O server.crt https://inventory.snets.it/ocsreports/server.crt --no-check-certificate 
sleep 10

# Edit configuration file

cat << EOM > /etc/ocsinventory/ocsinventory-agent.cfg
server=https://inventory.snets.it/ocsinventory
proxy=http://proxy:8080
AuthRequired=0
SSL=1
tag=$TAG
ca=/usr/share/szme-fuss-fog/ocs/server.crt
logfile=/var/log/ocsinventory-agent.log
EOM
